/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.google.gson.Gson;
import entity.Comment;
import entity.Image;
import entity.User;
import static java.lang.Integer.parseInt;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Seraph
 */
@Stateless
@Path("entity.comment")
public class CommentFacadeREST extends AbstractFacade<Comment> {

    @PersistenceContext(unitName = "WebApplication3PU")
    private EntityManager em;

    public CommentFacadeREST() {
        super(Comment.class);
    }

    @POST
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    public void create(@FormParam("comment") String comment, @FormParam("img-id") String imageId, @FormParam("comment-user-id") String userID) {
        Date currentDate = new Date();
        Comment entity = new Comment();
        User user = new User(parseInt(userID));
        entity.setCtime(currentDate);
        entity.setTime(new Timestamp(currentDate.getTime()));
        entity.setUser(user);
        Image img = em.find(Image.class, parseInt(imageId));
        entity.setCment(comment);
        entity.setImg(img);
        super.create(entity);
    }
    
    @GET
    @Path("checkComments/")
    public String checkComments() {
        List<Comment> commentList = em.createNamedQuery("Comment.findAll").getResultList();
        List<Image> imgList = em.createNamedQuery("Image.findAll").getResultList();
        List<String> result = new ArrayList<>();
        for (Image i : imgList) {
            int count = 0;
            for (Comment c : commentList) {
                if (i.getIid().equals(c.getImg().getIid())) {
                    count++;
                }
            }
            String item = ("{`imgId` : " + "`" + i.getIid() + "`, `cmtCount`:" + count + "}").replace('`', '"');
            result.add(item);
        }
        Gson gson = new Gson();
        return gson.toJson(result);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Comment entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Comment find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Comment> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Comment> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
