package servlet;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import entity.*;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Integer.parseInt;
import java.util.Date;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.transaction.UserTransaction;



@WebServlet(name = "upload", urlPatterns = {"/upload"})
@MultipartConfig(location="/var/www/html/upload")
public class UploadServlet extends HttpServlet {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("WebApplication3PU");
    EntityManager em;
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String fname = getFilename(request.getPart("file"));
        try {
            request.getPart("file").write(getFilename(request.getPart("file")));
            response.sendRedirect(request.getHeader("Referer"));
        } catch (Exception e) {
            out.println("Exception -->" + e.getMessage());
        } 
        try {
            //Transaction begin
            Date date = new Date();
            UserTransaction transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
            transaction.begin();
            em = emf.createEntityManager();
            String userIdParameter = request.getParameter("userID");
            int userID = 1;
            if (!userIdParameter.isEmpty()) {
                userID = Integer.parseInt(userIdParameter);
            }
            User useri = (User) em.find(User.class, userID);
            Image pic = new Image(fname, date, useri);
            useri.getImageCollection().add(pic);
            em.persist(pic);
            transaction.commit();
            
            
        } catch (Exception e) {
            out.println("Error with database" + e.getMessage());
        } 
    }
    
    //from http://stackoverflow.com/questions/2422468/how-to-upload-files-to-server-using-jsp-servlet/2424824#2424824
    public static String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                int fileNameLength = 6;
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                String charseq = "qwertyuiopasdfghjklzxcvbnm";
                
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }
}
